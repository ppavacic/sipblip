#pragma once
#include <pjsua.h>
#include <map>
#include <memory>
#include <string>
#include <glibmm-2.4/glibmm/dispatcher.h>
#include "Call.hpp"
#include "Conversation.hpp"

class SipAccount {
public:
	SipAccount(
		const std::string& sip_id, const std::string& reg_uri, const std::string& realm,
		const std::string& username, const std::string& password, const pjsua_transport_config& rtp_cfg,
		int password_type = 0, const std::string& scheme = "Digest",
	 	int cred_count = 1 //default plain text TODO
	);
	~SipAccount();
	std::string get_sipid() const;
	//non copyable
	SipAccount(const SipAccount& other) = delete;
	SipAccount& operator=(const SipAccount& other) = delete;

	void start_audio_call(std::string dst_uri, const pjsua_msg_data &msg_data);

	Call* get_call();
	void answer_call();
	void hangup_call();
	void send_im(std::string dst_uri, std::string message) const; //im's should be sent from conversations
	void send_im(std::string message); 	//instant message in started call as opposed to SipAccount::send_im

	void set_online();
	void set_offline();
	void reg_acc();
	void unreg_acc();

	//slot to be connected to incomming_call function handler in Call.hpp
	static void incoming_call_slot(
		pjsua_acc_id acc_id,
		pjsua_call_id call_id,
	    pjsip_rx_data* rdata
	);

	static void call_state_changed_slot(
		pjsua_acc_id acc_id,
		pjsua_call_id call_id
	);

	pjsua_acc_id get_id() { return acc_id; };

	Conversation* get_conversation(int index) { return &(conversations[index]); };
	void add_conversation(std::string sip_id);
	std::vector<std::string> get_conversation_names();

//SIGNALS
	Glib::Dispatcher signal_conversation_added;
	Glib::Dispatcher signal_call_changed;
	static void on_pager(const std::string sender, const std::string receiver, const std::string msg_txt);
private:
	std::string sip_id, reg_uri, realm, username, password, scheme;
	int password_type, cred_count;

    pjsua_acc_config acc_config;
    pjsua_acc_id acc_id;

	//TODO: tehnical debt, should be vector of calls
    std::unique_ptr<Call> call;
	void add_call(std::unique_ptr<Call> call); //

	std::vector<Conversation> conversations;

	//all accounts that have been generated,
	//used in incomming_call_slot and call_state_changed_slot to route call to accounts correctly
	inline static std::map<pjsua_acc_id, SipAccount*> sip_accounts;
	//used in on_pager for routing message to the correct account #TODO: currently we assume we
	//have only one account, same as sip_accouts just using sip address as key, points to same objects in memory
	inline static std::map<const std::string, SipAccount*> sip_accounts_2;
};
