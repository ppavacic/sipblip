#pragma once
#include <iostream>
#include <sigc++-2.0/sigc++/signal.h>
#include <string>
#include <pjsua.h>
#include <functional>
#include <map>

class SipAccount;

class Call {

public:
	//this must be called before anything can be used, TODO: remove this
	//and make routes not use signals
	static void init(
		pjmedia_port *ringback_port,
		std::function<void(pjsua_acc_id, pjsua_call_id, pjsip_rx_data*)> incoming_call_slot,
		std::function<void(pjsua_acc_id, pjsua_call_id)> call_state_changed_slot
	);

	Call(SipAccount& account, pjsua_call_id call_id = -1, std::string dst_sipid = "") :
	account(account), call_id(call_id), participant_sipid(dst_sipid) {};

	~Call() { hangup(); };

	Call& operator=(const Call& other) = delete;
	Call(const Call& other) = delete;

	void start_call(std::string dst_sipid, const pjsua_msg_data &msg_data);

	void answer() const;


	std::string get_participant_sipid() const { return participant_sipid; };

	void send_im(std::string message) {
		pj_str_t message_pj = pj_str(message.data());
		pjsua_call_send_im(call_id, NULL, &message_pj, NULL, NULL);
	};

	static void on_call_state(pjsua_call_id call_id, pjsip_event *e);
	static void on_call_media_state(pjsua_call_id call_id);
	static void on_incoming_call(pjsua_acc_id acc_id, pjsua_call_id call_id, pjsip_rx_data *rdata);
	static void on_call_audio_state(pjsua_call_id call_id);

private:
	void hangup();
	SipAccount& account;
	pjsua_call_id call_id;
	std::string participant_sipid;

	pjsua_call_setting call_options;
    inline static pjsua_call_setting call_settings;

	inline static pjmedia_port *ringback_port = nullptr;
	//EU standard
	static const int RINGBACK_CNT = 2;
	static const int RINGBACK_FREQ_F1 = 400;
	static const int RINGBACK_FREQ_F2 = 450;
	static const int RINGBACK_ON_MSEC = 400;
	static const int RINGBACK_OFF_MSEC = 200;
	static const int RINGBACK_INTERVAL = 2000;

	static const int RINGBACK_FREQ_US = 440;
	inline static pjmedia_tone_desc tones[1];
	enum TONES{RINGBACK};

    inline static sigc::signal<void,
    	pjsua_acc_id, pjsua_call_id, pjsip_rx_data*
    	>incomming_call_signal;

    inline static sigc::signal<void,
    	pjsua_acc_id, pjsua_call_id
    	>call_state_changed_signal;

	inline static std::map<pjsua_call_id, Call*> active_calls;
};
