#pragma once

#include <iostream>
#include <memory>
#include <vector>
#include <pjsua.h>
#include "SipAccount.hpp"

class App {
public:
	static App *get_instance();
	int run(int argc, char **argv);
	~App();
	int add_account(
		const std::string& sip_id, const std::string& reg_uri, const std::string& realm,
		const std::string& username, const std::string& password
	);
	sigc::signal<void, int> signal_account_list_changed; //emited when account is added or deleted
	SipAccount* get_account(int index) const;
private:
	App();
	static inline App *app = nullptr;

    std::string public_addr;
	bool is_running = false;

	pjsua_transport_id sip_id, rtp_id, tcp_id = -1;
    pjsua_transport_config sip_cfg, rtp_cfg;

    pjsua_config pjsua_cfg;
    pjsua_logging_config log_cfg;
    pjsua_media_config media_cfg;

	pj_pool_t *app_pool;

    pjsua_msg_data msg_data;

    //I don't know how to make type moveable so this is my solution
    //SipAccount can't be copyable since deconstructor deallocates sip account
    std::vector<std::unique_ptr<SipAccount>> sip_accounts;
	pj_str_t ringback_name;
	pjmedia_port *ringback_port;
};
