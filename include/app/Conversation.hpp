#pragma once
#include <vector>
#include <map>
#include <string>
#include <pj/string.h>
#include <pjsua.h>
#include <memory>
#include <glibmm-2.4/glibmm/dispatcher.h>

class SipAccount;

//TODO implement move, remove unique_ptr from glib::dispatcher
class Conversation {
public:
	Conversation(SipAccount &sip_acc, std::vector<std::string> participants, std::string conv_name  = "");
	Conversation(SipAccount &sip_acc, std::string participant, std::string conv_name  = "");
	void send_message(std::string message);
	void add_message(std::string sender, std::string message);
	void start_audio_call();
	unsigned int get_msg_num();
	std::string get_name() const;

	static void on_pager(
		pjsua_call_id call_id, const pj_str_t *from,
		const pj_str_t *to, const pj_str_t *contact,
	    const pj_str_t *mime_type, const pj_str_t *text
	);

	std::vector<std::string>* get_messages();
	std::vector<int>* get_messages_sender();
	std::string get_last_message();
	std::unique_ptr<Glib::Dispatcher> signal_message_added;
private:
	std::string conversation_name;
	std::vector<std::string> messages;
	std::vector<int> messages_sender; //sender 0 is SipAccount owning this conversation
	std::vector<std::string> participants; //TODO: multi user chat, idk how it works
	SipAccount &sip_account; //sip account that owns this conversation

	//later add group conversations
	/*std::vector<std::string> messages_sender;
	*/
};
