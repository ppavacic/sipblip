#pragma once
#include <gtkmm-3.0/gtkmm/window.h>
#include <gtkmm-3.0/gtkmm/application.h>
#include <gtkmm-3.0/gtkmm/button.h>
#include <gtkmm-3.0/gtkmm/builder.h>
#include <gtkmm-3.0/gtkmm/entry.h>
#include <gtkmm-3.0/gtkmm/listbox.h>
#include <gtkmm-3.0/gtkmm/label.h>
#include <gtkmm-3.0/gtkmm/comboboxtext.h>
#include <gtkmm-3.0/gtkmm/infobar.h>
#include <sigc++-2.0/sigc++/signal.h>

#include "App.hpp"

class App;

class GtkApp : public Gtk::Application {
public:
	GtkApp(App *pj_app);
private:

	Gtk::Window *window;
	Glib::RefPtr<Gtk::Builder> builder;
	App *pj_app;
	Gtk::Button
		*reg_add_acc, *conv_msg_snd, *conv_call,
		*call_end, *call_answer;
	Gtk::Entry *conv_msg_txt, *conv_add;
	Gtk::ComboBoxText *acc_picker;
	Gtk::ListBox *conv_list, *msg_list;
	Gtk::InfoBar *call_info_bar;
    Gtk::Label *conv_name;

	SipAccount* get_active_account();
	Conversation* get_active_conversation();
	Call* get_active_call();

	void set_active_conversation(int index);
	void swap_account(int acc_index);

	int active_account = -1;
	int active_conversation = -1;
	std::vector<Gtk::Label> conv_name_labels;
	std::vector<Gtk::Label> msg_list_labels;

	sigc::connection conv_added_conn, call_changed_conn, message_added_conn;
};
