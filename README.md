## SipBlip

SIP client written in C++, using GTK+ and pjsip.  
It allows you to call and send messages to users on the same network via SIP protocol.

## User interface

![Login form](https://gitlab.com/ppavacic/res/-/raw/main/SipBlip/SipBlip%20-%20SIP%20login.png)  
![SipBlip UI](https://gitlab.com/ppavacic/res/-/raw/main/SipBlip/SipBlip%20-%20UI.png)

## Technical stuff

### Todo:

#### High priority

*   [x] Message receive
*   [ ] Add a ringing sound when calling
*   [x] Displaying conversation messages
*   [x] There is some loopback audio in the audio call
*   [x] Call receive
*   [x] Multithreading problem
*   [x] When no Conversation is selected, disable the buttons
*   [ ] SIP over TLS
*   [ ] SRTP instead of RTP

#### Low priority

*   [ ] Conversations and Accounts should be using smart ptr, currently when vector  
    is reallocated, it might create problems in the future
*   [ ] Saving messages, accounts
*   [ ] On account change change conversation list
*   [ ] Setting online/offline status
*   [ ] Display call info things only when the call state is changed to  
    PJSIP\_INV\_STATE\_CONFIRMED instead of when the call is initiated
*   [x] Adding conversation changes on GTK GUI should be handled by signals instead of adding it manually
