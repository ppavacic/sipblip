#include <pjlib.h>
#include <pj/os.h>
#include <pjsip.h>
#include <pjsua.h>
#include <pjsip_ua.h>
#include <stdio.h>

#include "MainConf.hpp"
#include "App.hpp"

#ifdef GTK_UI
#include "GtkApp.hpp"
#endif

#define THIS_FILE "main.cpp"

int main(int argc, char **argv) {

    return pj_run_app([](int argc, char **argv) {
        App *app = App::get_instance();
        #ifdef GTK_UI
            PJ_LOG(2, (THIS_FILE, "gtk interface starting"));
            auto gtk_app = std::make_unique<GtkApp>(app);
            gtk_app->run();
        #else
            PJ_LOG(2, (THIS_FILE, "cli interface starting"));
            //pj_status_t status = app->run(argc, argv);
        #endif
        //delete app;
        return 0;
	}, argc, argv, 0);
}
