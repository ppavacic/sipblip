#include <gtkmm-3.0/gtkmm/entry.h>
#include <gtkmm-3.0/gtkmm/treemodelcolumn.h>
#include <gtkmm-3.0/gtkmm/liststore.h>
#include <gtkmm-3.0/gtkmm/stack.h>
#include <glib.h>
#include <stdexcept>
#include "GtkApp.hpp"
#include "MainConf.hpp"

GtkApp::GtkApp(App *pj_app) : Gtk::Application(APP_ID, Gio::APPLICATION_FLAGS_NONE),
pj_app(pj_app)

{
//INITIAL SETUP, WINDOW SETUP
    builder = Gtk::Builder::create_from_file(std::string(RESOURCE_DIR) + "/MainWindow.ui");
    builder->get_widget("main_window", window);
    builder->get_widget("acc_picker", acc_picker);
    builder->get_widget("reg_add_acc", reg_add_acc);
    builder->get_widget("conv_msg_snd", conv_msg_snd);
    builder->get_widget("conv_msg_txt", conv_msg_txt);
    builder->get_widget("conv_list", conv_list);
    builder->get_widget("conv_add", conv_add);
    builder->get_widget("conv_call", conv_call);
    builder->get_widget("call_info_bar", call_info_bar);
    builder->get_widget("call_end", call_end);
    builder->get_widget("call_answer", call_answer);
    builder->get_widget("msg_list", msg_list);
    builder->get_widget("conv_name", conv_name);


    this->conv_name_labels.reserve(10); //del later, replace with vector of uniques
    this->signal_activate().connect([this]{
        this->add_window(*window);
        window->present();
    });

    msg_list_labels.reserve(200);

//CONVERSATION STUFF
    //which conversation to display
    conv_list->signal_selected_rows_changed().connect([this] {
        set_active_conversation(conv_list->get_selected_row()->get_index());
    });

    //conversation creating new conversations
    conv_add->signal_activate().connect([this] {
        std::string conv_member_sip_id = this->conv_add->get_text();
        this->get_active_account()->add_conversation(conv_member_sip_id);
        this->conv_add->set_text("");
    });

    //chosing which account to display or to display acc_add_view
    acc_picker->signal_changed().connect([this]() {
    	Gtk::Stack *main_stack;
    	Gtk::Widget *widget_to_display;
	    builder->get_widget("main_stack", main_stack);
        int acc_id = this->acc_picker->get_active_row_number() -1; //0 is "add account", accounts start at 1
        if(acc_id == -1) builder->get_widget("acc_add_view", widget_to_display);
        else {
            builder->get_widget("conversation_view", widget_to_display);
            this->swap_account(acc_id);
        }
        main_stack->set_visible_child(*widget_to_display);
    });

    conv_call->signal_clicked().connect([this] {
        if(this->get_active_conversation() == nullptr) return;
        this->get_active_conversation()->start_audio_call();
    });

    //send message
    conv_msg_snd->signal_clicked().connect([this] () {
        if(this->get_active_conversation() == nullptr) return;
        get_active_conversation()->send_message(conv_msg_txt->get_text());
        conv_msg_txt->set_text("");
    });
    conv_msg_txt->signal_activate().connect([this] () {
        if(this->get_active_conversation() == nullptr) return;
        get_active_conversation()->send_message(conv_msg_txt->get_text());
        conv_msg_txt->set_text("");
    });

//REGISTER PAGE
    //when add_account is clicked (reg_add_acc)
    reg_add_acc->signal_clicked().connect([this] (){

        Gtk::Entry *sip_id, *sip_registrar, *sip_realm,
            *sip_username, *sip_password;

        builder->get_widget("reg_sip_id", sip_id);
        builder->get_widget("reg_sip_registrar", sip_registrar);
        builder->get_widget("reg_sip_realm", sip_realm);
        builder->get_widget("reg_sip_username", sip_username);
        builder->get_widget("reg_sip_password", sip_password);

        try {
            this->pj_app->add_account(
                sip_id->get_text(), sip_registrar->get_text(),
                sip_realm->get_text(), sip_username->get_text(),
                sip_password->get_text()
            );
        } catch (int param) {
            std::cout << "Error: " << param << std::endl;
        }

        sip_id->set_text("");
        sip_registrar->set_text("");
        sip_realm->set_text("");
        sip_username->set_text("");
        sip_password->set_text("");
    });

    //after adding accounts, update acc_picker list
    pj_app->signal_account_list_changed.connect([this](int acc_index){
        this->acc_picker->append(this->pj_app->get_account(acc_index)->get_sipid());
        this->swap_account(acc_index);
    });
//CALL stuff
    call_end->signal_clicked().connect([this] {
        get_active_account()->hangup_call();
    });

    call_answer->signal_clicked().connect([this] {
        get_active_account()->answer_call();
    });

    //TODO: delete
    this->pj_app->add_account(
        "100@192.168.0.8", "192.168.0.8",
        "asterisk", "100",
        "100"
    );
}

Call* GtkApp::get_active_call() {
    return this->pj_app->get_account(active_account)->get_call();
}


SipAccount* GtkApp::get_active_account() {
    if(active_account < 0) return nullptr;
    return this->pj_app->get_account(active_account);
}

Conversation* GtkApp::get_active_conversation() {
    if(active_account < 0 || active_conversation < 0) return nullptr;
    return get_active_account()->get_conversation(active_conversation);
}

void GtkApp::set_active_conversation(int index) {
    active_conversation = index;//swaps which account is active
    conv_name->set_text(get_active_conversation()->get_name());
    conv_name->set_text(get_active_conversation()->get_name());
    //TODO: chech whether this is propper way of doing this
    for(auto &child_widget : msg_list->get_children()) {
        msg_list->remove(*child_widget);
    }

    msg_list_labels.clear(); // clear old text messages, TODO: clear but without realloc
    int i = 0;
    Gtk::Label test_delete;
    auto &sender_list = *get_active_conversation()->get_messages_sender(); // 0 is local user
    for(auto &msg : *get_active_conversation()->get_messages()) {
        msg_list_labels.emplace_back(msg);
        msg_list_labels.back().set_halign(sender_list[i++] == 0 ? Gtk::ALIGN_END : Gtk::ALIGN_START);
        msg_list_labels.back().set_selectable(true);
        msg_list->add(msg_list_labels.back());
    }
    msg_list->show_all();

    //action for adding single message
    if(message_added_conn.connected()) message_added_conn.disconnect();
    message_added_conn = get_active_conversation()->signal_message_added->connect([this] {
        //TODO add checks whether message was actually added, just double check to be sure
        auto active_conv = this->get_active_conversation();
        auto msg = active_conv->get_last_message();
        auto &sender_list = *get_active_conversation()->get_messages_sender(); // 0 is local user
        msg_list_labels.emplace_back(msg);
        msg_list_labels.back().set_halign(sender_list[active_conv->get_msg_num() -1] == 0 ? Gtk::ALIGN_END : Gtk::ALIGN_START);
        msg_list_labels.back().set_selectable(true);
        msg_list->add(msg_list_labels.back());
        msg_list_labels.back().show();
    });
}

void GtkApp::swap_account(int acc_index) {
//TODO: remove old connection if exists to signal for old acc
    active_account = acc_index;
    int entry_index = acc_index +1; // 0 is "add account" in Gtk::acc_picker, accs start at 1
    acc_picker->set_active(entry_index);
    get_active_call() ? call_info_bar->show_all() : call_info_bar->hide(); //if call exists in new account display call options
    auto new_acc = get_active_account();

    //delete old thread connections
    if(call_changed_conn.connected()) call_changed_conn.disconnect();
    if(conv_added_conn.connected()) conv_added_conn.disconnect();

    //connect signal if call object changes; if there is incoming call
    call_changed_conn = new_acc->signal_call_changed.connect([this] {
        auto active_call = get_active_call();
        if(active_call == nullptr) {
            call_info_bar->hide();
        } else {
            Gtk::Label *call_name;
            builder->get_widget("call_name", call_name);
            call_name->set_text(active_call->get_participant_sipid());
            call_info_bar->show_all();
        }

    });

    //adding new element to conversation list after it has been added
    conv_added_conn = conv_added_conn = get_active_account()->signal_conversation_added.connect([this]() {
        auto new_conv_name = this->get_active_account()
                                 ->get_conversation_names()
                                 .back();
        this->conv_name_labels.emplace_back(new_conv_name);
        auto *new_element = &this->conv_name_labels.back();
        new_element->set_visible(true);
        this->conv_list->append(*new_element);
    });

    //update conversation list
    /*conv_list->
    for(auto &conv : get_active_account()->get_conversation_names()) {

    }*/
}
