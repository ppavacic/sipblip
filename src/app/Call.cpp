#include "Call.hpp"
#include "SipAccount.hpp"
#define THIS_FILE "Call.cpp"

void Call::hangup() {
	pj_str_t reason = pj_str((char*)"User ended session!");

    pjsua_call_hangup(call_id, 0, &reason, NULL);
}

void Call::answer() const {
    pjsua_call_answer2(call_id, &call_settings, 200, NULL, NULL);
};

void Call::init(
    pjmedia_port *ringback_port,
	std::function<void(pjsua_acc_id, pjsua_call_id, pjsip_rx_data*)> incoming_call_slot,
	std::function<void(pjsua_acc_id, pjsua_call_id)> call_state_changed_slot
) {
    Call::ringback_port = ringback_port;
	tones[0].freq1 = RINGBACK_FREQ_F1;
	tones[0].freq2 = RINGBACK_FREQ_F2;
	tones[0].on_msec = RINGBACK_ON_MSEC;
	tones[0].off_msec = RINGBACK_OFF_MSEC;

	//overengineering, I wanna use signals, TODO: gotta add GMainThread object
	incomming_call_signal.connect(incoming_call_slot);
	call_state_changed_signal.connect(call_state_changed_slot);

    pjsua_call_setting_default(&call_settings);
    call_settings.aud_cnt = 1;
    call_settings.vid_cnt = 0;
}

//static route functions SIGNALS, signals that are emited here are handled by slots passed in Call::init()
//e.g  std::function incoming_call_slot, call_state_changed_slot etc..
void Call::on_incoming_call(
	pjsua_acc_id acc_id, pjsua_call_id call_id,
    pjsip_rx_data *rdata
) {
	//emits signal that should handled by SipAccount::incomming_call_slot
	//so that correct user creates incomming call object
	incomming_call_signal.emit(acc_id, call_id, rdata);
}

//STATIC, ROUTE FUNCTIONS
void Call::on_call_state(pjsua_call_id call_id, pjsip_event *e) {
    pjsua_call_info call_info;
    pjsua_call_get_info(call_id, &call_info);

    call_state_changed_signal.emit(call_info.acc_id, call_id);
    /*pjsua_conf_port_id call_conf_port = call_info.media[0].stream.aud.conf_slot;
    pjsua_conf_connect(call_conf_port, 0);*/
    //pjsua_conf_connect(0, call_conf_port);
}

void Call::on_call_media_state(pjsua_call_id call_id) { 	//function doesn't get called
	PJ_LOG(2, (THIS_FILE, "CALL MEDIA STATE"));
}

void Call::on_call_audio_state(pjsua_call_id call_id) {
    pjsua_call_info call_info;
    pjsua_call_get_info(call_id, &call_info);

    //TODO: disconnect this
    auto call_conf_port = call_info.media[0].stream.aud.conf_slot;
	pjsua_conf_connect(call_conf_port, 0);
	//pjsua_conf_connect(0, call_conf_port);
}

void Call::start_call(std::string dst_sip_id, const pjsua_msg_data &msg_data) {
    participant_sipid = dst_sip_id;
    dst_sip_id = "sip:" + dst_sip_id;
    pj_str_t dst_sip_id_pj = pj_str(dst_sip_id.data());
	pjsua_call_make_call(account.get_id(), &dst_sip_id_pj, &call_settings, NULL, &msg_data, &call_id);
};
