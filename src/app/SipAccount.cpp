#include "SipAccount.hpp"
#include <pjsua.h>
#include <glibmm/ustring.h>
#include <iostream>

#define THIS_FILE "SipAccount.cpp"

SipAccount::SipAccount(
    const std::string& sip_id, const std::string& reg_uri, const std::string& realm,
	const std::string& username, const std::string& password, const pjsua_transport_config& rtp_cfg,
	int password_type, const std::string& scheme,
	int cred_count
) :
    sip_id(sip_id), reg_uri(reg_uri), realm(realm), username(username), password(password),
    scheme(scheme), password_type(password_type), cred_count(cred_count)
 {
    std::string sip_id_full = "sip:" + sip_id;
    std::string reg_uri_full = "sip:" + reg_uri;
    pjsua_acc_config_default(&acc_config);

    acc_config.rtp_cfg = rtp_cfg;
    acc_config.id = pj_str(sip_id_full.data());
    acc_config.reg_uri = pj_str(reg_uri_full.data());
    acc_config.cred_count = cred_count;
    acc_config.cred_info[0].scheme = pj_str(this->scheme.data());
    acc_config.cred_info[0].realm = pj_str(this->realm.data());
    acc_config.cred_info[0].username = pj_str(this->username.data());
    acc_config.cred_info[0].data = pj_str(this->password.data());
    acc_config.cred_info[0].data_type = password_type;

    pj_status_t status = pjsua_acc_add(&this->acc_config, PJ_TRUE, &acc_id);
    if(status) throw 401;
    sip_accounts.insert({acc_id, this});
    //pjsua_acc_set_user_data(0, void *user_data); // set user data

}

SipAccount::~SipAccount() {
    pjsua_acc_del(acc_id);
    sip_accounts.erase(acc_id);
}

std::string SipAccount::get_sipid() const {
    return sip_id;
}

void SipAccount::add_call(std::unique_ptr<Call> call) {
    if(this->call) {
        std::cout << "Call already in progress! End this call and try again" << std::endl;
    } else {
        this->call = std::move(call);
        signal_call_changed.emit();
    }
}
#include <functional>
void SipAccount::add_conversation(std::string sip_id) {
	if(sip_id.empty()) return;
	conversations.emplace_back(*this, sip_id);
	std::cout << "Emiting conv" << std::endl;
	signal_conversation_added.emit();
};

void SipAccount::start_audio_call(
    std::string dst_uri,
    const pjsua_msg_data &msg_data
) {
    if(this->call) {
        std::cout << "Call already in progress! End this call and try again" << std::endl;
    } else {
        call = std::make_unique<Call>(*this);
        call->start_call(dst_uri, msg_data);
        signal_call_changed.emit();
    }

    //pjsua_call_make_call(acc_id, &dst_uri_pjstr, &call_settings, NULL, &msg_data, &call_id);
}

void SipAccount::send_im(std::string dst_uri, std::string message) const {
    dst_uri = "sip:" + dst_uri;
    pj_str_t message_pj = pj_str(message.data());
    pj_str_t dst_uri_pj = pj_str(dst_uri.data());
    pjsua_im_send(acc_id, &dst_uri_pj, NULL, &message_pj, NULL, NULL);
}

void SipAccount::send_im(std::string message) {
    if(call) call->send_im(message);
    else PJ_LOG(2, (THIS_FILE, "Tried to send call message to closed call!"));
}

void SipAccount::set_offline() {
	pjsua_acc_set_online_status(acc_id, PJ_FALSE);
}

void SipAccount::set_online() {
	pjsua_acc_set_online_status(acc_id, PJ_TRUE);

}

void SipAccount::reg_acc() {
    pjsua_acc_set_registration(acc_id, PJ_TRUE);
}

void SipAccount::unreg_acc() {
    pjsua_acc_set_registration(acc_id, PJ_FALSE);
}

void SipAccount::hangup_call() {
    if(call) call = nullptr;
    else PJ_LOG(2, (THIS_FILE, "Tried to hangup non-existent call!"));
    signal_call_changed.emit();
}

void SipAccount::answer_call() {
    call->answer();
}

std::vector<std::string> SipAccount::get_conversation_names() {
    std::vector<std::string> conv_names(5);
    for(auto &conversation : conversations) {
        conv_names.push_back(conversation.get_name());
    }
    return conv_names;
}

Call* SipAccount::get_call() {
    return call.get();
}

//SLOTS, ROUTES
void SipAccount::incoming_call_slot(
	pjsua_acc_id acc_id,
	pjsua_call_id call_id,
    pjsip_rx_data* rdata
) {
    pjsua_call_info call_info;
    pjsua_call_get_info(call_id, &call_info);
    std::string caller_sipid(call_info.remote_info.ptr);

    caller_sipid = caller_sipid.substr(5, call_info.remote_info.slen -6);

	sip_accounts[acc_id]->add_call(
	    std::make_unique<Call>(*sip_accounts[acc_id], call_id, caller_sipid)
	);
}

void SipAccount::call_state_changed_slot(
	pjsua_acc_id acc_id,
	pjsua_call_id call_id
) {
    pjsua_call_info call_info;
    pjsua_call_get_info(call_id, &call_info);
    auto acc = sip_accounts[acc_id];

    PJ_LOG(3, (THIS_FILE, "PP Call state changed to: %d", call_info.state));

    switch(call_info.state) {
        case PJSIP_INV_STATE_DISCONNECTED:
            acc->hangup_call();
        break;
        case PJSIP_INV_STATE_CONFIRMED:
    		//pjsua_conf_connect(call_info.media[1].stream.aud.conf_slot, 0);
        break;
        default:
        break;
    }
}

void SipAccount::on_pager(const std::string sender, const std::string receiver, const std::string msg_txt) {
    //TODO: working with 1 account, should use std::map to get which acc to use
    auto *acc = sip_accounts[0];


    //TODO: create std::map of conversations with conv names as keys
    bool message_added_to_conv = false;
        auto sender_sipid = sender.substr(5, sender.length() -6);
    add_msg_to_conv:
    for(auto &conv : acc->conversations) {
        if(conv.get_name() == sender_sipid) {
            conv.add_message(sender_sipid, msg_txt);
            message_added_to_conv = true;
        }
    }

    // if conversation with that person doesn't exist create new conversation
    if(!message_added_to_conv) {
        acc->add_conversation(sender_sipid);
        goto add_msg_to_conv; //TODO: remove because its goto
    }
}
