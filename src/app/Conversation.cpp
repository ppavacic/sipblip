#include "Conversation.hpp"
#include "SipAccount.hpp"
#include <pjsua.h>
#include <pjsip.h>

#define THIS_FILE "Conversation.cpp"

Conversation::Conversation(SipAccount &sip_acc, std::vector<std::string> member_addresses, std::string conv_name) :
    sip_account(sip_acc)
{
    messages.reserve(100);
    this->participants = member_addresses;
    signal_message_added = std::make_unique<Glib::Dispatcher>();

    if(conv_name == "") conv_name = member_addresses[0];
    else conversation_name = conv_name;
    //participants.push_back(my_sip_addr);
}

Conversation::Conversation(SipAccount &sip_acc, std::string participant, std::string conv_name) :
    sip_account(sip_acc)
{
    signal_message_added = std::make_unique<Glib::Dispatcher>();
    this->participants.push_back(participant);
    if(conv_name.empty()) conversation_name = participant;
    else conversation_name = conv_name;
}

void Conversation::add_message(std::string sender, std::string message) {
    messages_sender.push_back(1);
    messages.push_back(message);
    signal_message_added->emit();
}

void Conversation::send_message(std::string message) {
    sip_account.send_im(participants[0], message);

    //TODO: should only add IM on succesfull send
    messages.push_back(message);
    messages_sender.push_back(0);
    signal_message_added->emit();
}

std::string Conversation::get_name() const {
    return conversation_name;
}


void Conversation::start_audio_call() {
    pjsua_msg_data msg_data;
    pjsua_msg_data_init(&msg_data);
    msg_data.msg_body = pj_str((char*)"I would love to talk with you!");
    sip_account.start_audio_call(participants[0], msg_data);
}

std::vector<std::string>* Conversation::get_messages() {
    return &messages;
}

std::vector<int>* Conversation::get_messages_sender() {
    return &messages_sender;
}

std::string Conversation::get_last_message() {
    return messages.back();
}

//ROUTE
void Conversation::on_pager(
	pjsua_call_id call_id, const pj_str_t *from,
	const pj_str_t *to, const pj_str_t *contact,
    const pj_str_t *mime_type, const pj_str_t *text
) {
    SipAccount::on_pager(std::string(from->ptr), std::string(to->ptr), std::string(text->ptr));
}

unsigned int Conversation::get_msg_num() {
    return messages.size();
}
