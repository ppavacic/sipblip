#include <stdio.h>
#include "App.hpp"
#include "Call.hpp"


#define THIS_FILE "App.cpp"

App* App::get_instance() {
    if(app == nullptr) {
    	app = new App();
    	app->is_running = true;
    } else {
        PJ_LOG(2, (THIS_FILE, "App already allocated\n"));
    }
    fflush(stdin);
	return app;
}


int App::run(int argc, char **argv) {

    char option = '\0';
	while(option != 'e') {
	    std::cin >> option;
        //TODO remove testing data with real data
	    switch(option) {
	        case 'c':
    	        pjsua_msg_data_init(&msg_data);
	            sip_accounts[0]->start_audio_call("sip:101@192.168.0.8", msg_data);
	        break;
	        case 'C':
    	        pjsua_msg_data_init(&msg_data);
	            sip_accounts[1]->start_audio_call("sip:101@192.168.0.8", msg_data);
	        break;
	        case 'i':
    	        sip_accounts[0]->send_im("sip:101@192.168.0.8", "Hello world");
	        break;
	        case 'I':
    	        sip_accounts[0]->send_im("Hello from call");
	        break;
	        case '1':
	            sip_accounts[0]->set_online();
	        break;
	        case '0':
	            sip_accounts[0]->set_offline();
	        break;
	        case 'a':
	            sip_accounts[0]->answer_call();
	        break;
	        case 'r':
	            sip_accounts[0]->reg_acc();
	        break;
	        case 'u':
	            sip_accounts[0]->unreg_acc();
	        break;
	        case 'h':
	            sip_accounts[0]->hangup_call();
	        break;

	    }
	};

	return 0;
}

SipAccount* App::get_account(int index) const {
    if(index >= (int)sip_accounts.size()) return nullptr;
    return sip_accounts[index].get();
}

int App::add_account(
	const std::string& sip_id, const std::string& reg_uri, const std::string& realm,
	const std::string& username, const std::string& password
) {
    auto new_acc = std::make_unique<SipAccount>(
        sip_id, reg_uri, realm,
        username, password, rtp_cfg
    );
    sip_accounts.push_back(std::move(new_acc));

    int new_acc_index = sip_accounts.size() -1;
    signal_account_list_changed.emit(new_acc_index);
    return new_acc_index;
}

App::App() {
    //pjsua setup
    pj_status_t status = 0;

    pjsua_config_default(&pjsua_cfg);
    pjsua_logging_config_default(&log_cfg);
    pjsua_media_config_default(&media_cfg);

    pjsua_cfg.max_calls = 2;
    pjsua_cfg.cb.on_call_state = &Call::on_call_state;
    pjsua_cfg.cb.on_incoming_call = &Call::on_incoming_call;
    pjsua_cfg.cb.on_call_media_state = &Call::on_call_audio_state;
    pjsua_cfg.cb.on_pager = &Conversation::on_pager;

    media_cfg.channel_count = 1;
    media_cfg.max_media_ports = 1;


    pjsua_transport_config_default(&sip_cfg);
    pjsua_transport_config_default(&rtp_cfg);

    status += pjsua_create();
    status += pjsua_init(&pjsua_cfg, &log_cfg, &media_cfg);
    status += pjsua_start();


    app_pool = pjsua_pool_create("sipblip-pool", 1000, 1000);

    if(status > 0) PJ_LOG(2, (THIS_FILE, "Can't set up pjsua"));

    pjmedia_tonegen_create2(
        app_pool, &ringback_name, media_cfg.clock_rate,
        media_cfg.channel_count, 320, 16, PJMEDIA_TONEGEN_LOOP,
        &ringback_port
    );

    Call::init(ringback_port, &SipAccount::incoming_call_slot, &SipAccount::call_state_changed_slot);

    //configur e transport config
    public_addr = "192.168.0.2";
    sip_cfg.public_addr = pj_str(public_addr.data());
    sip_cfg.port = 5060;
    //https://www.pjsip.org/pjsip/docs/html/structpjsip__tls__setting.htm
    //sip_cfg.tls_setting.ca_list_file = pj_str("/home/ppavacic/Documents/NapredneMreze/sip-client/tmp/certificate.crt");
    //sip_cfg.tls_setting.privkey_file = pj_str("/home/ppavacic/Documents/NapredneMreze/sip-client/tmp/private.key");

    rtp_cfg.port = 4000;
    rtp_cfg.public_addr = pj_str(public_addr.data());
    pjsua_transport_create(PJSIP_TRANSPORT_UDP, &sip_cfg, &sip_id);

}
App::~App() {
    pjmedia_port_destroy(ringback_port);
    pj_pool_release(app_pool);
    pjsua_destroy();
}
